function _boot() {
    const form = document.getElementById("contact-us-submit-form");
    if (form !== null) {
        form.addEventListener('submit', function (e) {
            submit(e)
        });
    }
}

function activate(button) {
    if (!button.enabled) {
        button.className = "ask-form-button active";
        button.enabled = true;
    } else {
        button.className = "ask-form-button";
        button.enabled = false;
    }

    let list = activatedList();
    let submitButton = document.getElementById("submit-button");
    if (list != null && list.length !== 0) {
        submitButton.className = "send-button active";
        submitButton.disabled = false
    } else {
        submitButton.className = "send-button";
        submitButton.disabled = true
    }
}

function modal_success() {
    $(document).ready(function () {
        var inst = $('[data-remodal-id=modal_success]').remodal();
        inst.open();
    });
}

function modal_error() {
    $(document).ready(function () {
        var inst = $('[data-remodal-id=modal_failure]').remodal();
        inst.open();
    });
}

function activatedList() {
    return document.getElementsByClassName("ask-form-button active");
}

function submit(e) {
    e.preventDefault();
    let services = Array.prototype.map.call(activatedList(), function (button) {
        return button.getAttribute("data-field")
    });

    let servicesStr = services.join(', ');

    const text = document.getElementById("input-text").value;
    const company = document.getElementById("input-company").value;
    const name = document.getElementById("input-name").value;
    const email = document.getElementById("input-email").value;
    const position = document.getElementById("input-position").value;
    const phone = document.getElementById("input-phone").value;
    const website = document.getElementById("input-website").value;

    let mail = {
        from_name: 'no-reply@valoru.com',
        to_name: 'mail@valoru-software.com',
        subject: 'Contact Form message',
        message_html: text,
        email: email,
        name: name,
        company: company,
        position: position,
        phone: phone,
        website: website,
        service: servicesStr
    };

    const btn = document.getElementById("submit-button");
    btn.className = "send-button-inactive";
    btn.disabled = true;
    console.log("Sending email: " + mail);
    const responseStatusPromise = emailjs.send(
        'smtp_server',
        'template_y3Z6jnPx',
        mail,
        'user_qcvUAUANvRKmVmEKCw993'
    );

    responseStatusPromise.then(function(response) {
        if (response.status === 200) {
            console.info("Your request was sent");
            btn.className = "send-button active";
            btn.disabled = false;
            modal_success()
        } else {
            console.error("Failed to send request, please try later");
            modal_error()
        }
    });
}

_boot();