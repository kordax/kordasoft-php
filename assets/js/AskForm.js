function _boot() {
    const askButton = document.getElementById("ask-question-button");
    askButton.addEventListener('click', function () {
        show()
    });

    const form = document.getElementById("ask-submit-form");
    form.addEventListener('submit', function (e) {
        submit(e)
    });
}

function show() {
    $(document).ready(function () {
        var inst = $('[data-remodal-id=modal_ask_question]').remodal();
        inst.open();
    });
}

function modal_success() {
    $(document).ready(function () {
        var inst = $('[data-remodal-id=modal_success]').remodal();
        inst.open();
    });
}

function modal_error() {
    $(document).ready(function () {
        var inst = $('[data-remodal-id=modal_failure]').remodal();
        inst.open();
    });
}

async function submit(e) {
    e.preventDefault();

    const btn = document.getElementById("ask-submit-button");
    btn.className = "send-button processing";
    btn.disabled = true;

    const name = document.getElementById("ask-question-input-name").value;
    const text = document.getElementById("ask-question-input-text").value;
    const email = document.getElementById("ask-question-input-email").value;
    const phone = document.getElementById("ask-question-input-phone").value;
    const website = document.getElementById("ask-question-input-website").value;

    let response = fetch(
        '/submit-email',
        {
            method: 'post',
            body: JSON.stringify({
                form_name: name,
                form_text: text,
                form_email: email,
                form_phone: phone,
                form_website: website
            })
        });

    response
        .then(function (response) {
            btn.disabled = true;
            let js = response.json()

            if (response.status !== 200) {
                js.then(function (result) {
                    btn.className = "send-button error";
                    btn.disabled = false;
                    btn.textContent = "ERROR"
                    window.location.assign("contact-us?result=nok&error_message=" + result['message'])
                })
            } else {
                btn.textContent = "SUCCESS";
                window.location.assign("contact-us?result=ok")
            }
            return response;
        })
        .then(function (response) {
            return console.debug(response);
        })
        .catch(function (msg) {
            btn.className = "send-button error";
            btn.disabled = false;
            btn.textContent = "ERROR"
            window.location.assign("contact-us?result=nok&error_message=" + msg)
        });
}

_boot();