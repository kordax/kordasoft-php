/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './js/remodal/css/remodal.css'
import './js/remodal/css/remodal-default-theme.css'

import './styles/app.css';
import './styles/css/Company.css';
import './styles/css/ContactUs.css';
import './styles/css/Description.css';
import './styles/css/Error.css';
import './styles/css/FixedHeader.css';
import './styles/css/Footer.css';
import './styles/css/Header.css';
import './styles/css/Home.css';
import './styles/css/index.css';
import './styles/css/Inventory.css';
import './styles/css/Main.css';
import './styles/css/Std.css';
import './styles/css/animate/animate.css';
import './styles/css/form/SubmitOk.css';

import 'jquery';

import './js/wow/wow'

import './js/remodal/remodal'

import './js/Animations'
import './js/AskForm'
import './js/Form'
import './js/Slider'

// start the Stimulus application
import './bootstrap';
