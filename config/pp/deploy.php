<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer
{
    public function configure()
    {
        return $this->getConfigBuilder()
            ->webDir('public_html')
            ->composerInstallFlags('--prefer-dist -v')
            ->remotePhpBinaryPath('/usr/local/bin/php7.3 -d display_errors=on')
            ->useSshAgentForwarding(true)
            ->server('kordax@kordax.beget.tech')
            ->deployDir('/home/k/kordax/valoru-software.com')
            ->repositoryUrl('git@gitlab.com:kordax/kordasoft-php.git')
            ->remoteComposerBinaryPath('/usr/local/bin/php7.3 /usr/local/bin/composer')
            ->repositoryBranch('preprod');
    }

    // run some local or remote commands before the deployment is started
    public function beforeStartingDeploy()
    {
        // $this->runLocal('./vendor/bin/simple-phpunit');
    }

    // run some local or remote commands after the deployment is finished
    public function beforeFinishingDeploy()
    {
        // $this->runRemote('{{ console_bin }} app:my-task-name');
        // $this->runLocal('say "The deployment has finished."');
    }
};
