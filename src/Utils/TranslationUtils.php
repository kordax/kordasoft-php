<?php
namespace App\Utils;

use Symfony\Contracts\Translation\TranslatorInterface;

abstract class TranslationUtils {
    public static $_translations = [
        'home.image_text',
        'contact_us.email.success',
        'contact_us.email.error',
        'contact_us.ask_form.title',
        'contact_us.ask_form.name',
        'contact_us.ask_form.email',
        'contact_us.ask_form.company',
        'contact_us.ask_form.phone',
        'contact_us.ask_form.website',
        'contact_us.ask_form.send_button_value',
        'contact_us.ask_form.service_type.outsourcing',
        'locale',
        'header.inventory.home',
        'header.inventory.services',
        'header.inventory.about_us',
        'header.inventory.contact_us',
        'header.language.en',
        'header.language.ru',
        'header.language_selector',
        'footer.contact_us.ask_question',
        'footer.copyright',
        'footer.footer_text',
        'footer.bottom.privacy_policy',
        'footer.bottom.terms_of_use'
    ];

    public static function translate(array $entries, TranslatorInterface $translator, string $locale): array {
        $result = [];
        foreach ($entries as $entry) {
            $result[$entry] = $translator->trans($entry, [], null, $locale);
        }
        $result['locale'] = $locale;

        return $result;
    }
}
