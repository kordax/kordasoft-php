<?php

namespace App\Controller\Form;

use PHPUnit\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class SubmitEmail extends AbstractController {

    /**
     * @Route("/submit-email", name="submit_email")
     * @param MailerInterface $mailer
     * @param Request $request
     * @return Response
     */
    public function submit(LoggerInterface $logger, MailerInterface $mailer, Request $request) {
        $data = json_decode($request->getContent(), true);
        $form_name = $data['form_name'];
        $form_email = $data['form_email'];
        $form_phone = $data['form_phone'];
        $form_text = $data['form_text'];
        $form_website = $data['form_website'];

        $mailFrom = $this->getParameter('form.mail_from');
        $mailTo = $this->getParameter('form.mail_to');

        $email = (new TemplatedEmail())
            ->htmlTemplate('email/email_no_reply.html.twig')
            ->from($mailFrom)
            ->to($mailTo)
            ->subject('Contact form message. Please do not reply')
            ->context([
                'form_text' => $form_text,
                'form_phone' => $form_phone,
                'form_email' => $form_email,
                'form_name' => $form_name,
                'form_website' => $form_website,
                'ip_address' => $request->getClientIp()
            ]);

        try {
            $mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $logger->error("Error sending an email", ["exception" => $e]);

            return $this->json(data: ['message' => $e->getMessage()], status: 500, headers: ['Content-Type' => 'application/json;charset=UTF-8']);
        } catch (\Throwable $e) {
            $logger->error("Error sending an email", ["exception" => $e]);
        }

        return $this->json(data: ['message' => 'OK']);
    }
}