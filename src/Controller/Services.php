<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class Services extends AbstractController {

    /**
     * @Route("/services", name="services")
     * @param TranslatorInterface $translator
     * @param Request $request
     * @return Response
     */
    public function route(TranslatorInterface $translator, Request $request) {
        return $this->render('contact-us.html.twig',
            [
                'home.image_text' => $translator->trans('home.image_text', [], null, $request->getLocale()),
                'home.how' => $translator->trans('home.how', [], null, $request->getLocale()),
                'home.steps.design.body' => $translator->trans('home.steps.design.body', [], null, $request->getLocale()),
                'home.steps.design.header' => $translator->trans('home.steps.design.header', [], null, $request->getLocale()),
                'home.steps.implementation.body' => $translator->trans('home.steps.implementation.body', [], null, $request->getLocale()),
                'home.steps.implementation.header' => $translator->trans('home.steps.implementation.header', [], null, $request->getLocale()),
                'home.steps.qa.body' => $translator->trans('home.steps.qa.body', [], null, $request->getLocale()),
                'home.steps.qa.header' => $translator->trans('home.steps.qa.header', [], null, $request->getLocale()),
                'home.steps.support.body' => $translator->trans('home.steps.support.body', [], null, $request->getLocale()),
                'home.steps.support.header' => $translator->trans('home.steps.support.header', [], null, $request->getLocale()),
                'home.pros.why' => $translator->trans('home.pros.why', [], null, $request->getLocale()),
                'header.inventory.company' => $translator->trans('header.inventory.company', [], null, $request->getLocale()),
                'header.inventory.contacts' => $translator->trans('header.inventory.contacts', [], null, $request->getLocale()),
                'header.language.en' => $translator->trans('header.language.en', [], null, $request->getLocale()),
                'header.language.ru' => $translator->trans('header.language.ru', [], null, $request->getLocale()),
                'header.language_selector' => $translator->trans('header.language_selector', [], null, $request->getLocale()),
                'footer.contacts.ask_question' => $translator->trans('footer.contacts.ask_question', [], null, $request->getLocale()),
                'footer.copyright' => $translator->trans('footer.copyright', [], null, $request->getLocale()),
                'locale' => $request->getLocale(),
                'contacts.image_text' => $translator->trans('contacts.image_text', [], null, $request->getLocale()),
                'contacts.ask_form.title' => $translator->trans('contacts.ask_form.title', [], null, $request->getLocale()),
                'contacts.ask_form.service_type.title' => $translator->trans('contacts.ask_form.service_type.title', [], null, $request->getLocale()),
                'contacts.ask_form.service_type.support' => $translator->trans('contacts.ask_form.service_type.support', [], null, $request->getLocale()),
                'contacts.ask_form.service_type.qa' => $translator->trans('contacts.ask_form.service_type.qa', [], null, $request->getLocale()),
                'contacts.ask_form.service_type.development' => $translator->trans('contacts.ask_form.service_type.development', [], null, $request->getLocale()),
                'contacts.ask_form.budget.title' => $translator->trans('contacts.ask_form.budget.title', [], null, $request->getLocale()),
                'contacts.ask_form.info.title' => $translator->trans('contacts.ask_form.info.title', [], null, $request->getLocale()),
                'contacts.ask_form.name' => $translator->trans('contacts.ask_form.name', [], null, $request->getLocale()),
                'contacts.ask_form.email' => $translator->trans('contacts.ask_form.email', [], null, $request->getLocale()),
                'contacts.ask_form.company' => $translator->trans('contacts.ask_form.company', [], null, $request->getLocale()),
                'contacts.ask_form.phone' => $translator->trans('contacts.ask_form.phone', [], null, $request->getLocale()),
                'contacts.ask_form.position' => $translator->trans('contacts.ask_form.position', [], null, $request->getLocale()),
                'contacts.ask_form.website' => $translator->trans('contacts.ask_form.website', [], null, $request->getLocale()),
                'contacts.ask_form.send_button_value' => $translator->trans('contacts.ask_form.send_button_value', [], null, $request->getLocale()),
                'contacts.ask_form.service_type.outsourcing' => $translator->trans('contacts.ask_form.service_type.outsourcing', [], null, $request->getLocale()),
                'contacts.email.success' => $translator->trans('contacts.email.success'), [], null, $request->getLocale(),
                'contacts.email.error' => $translator->trans('contacts.email.error'), [], null, $request->getLocale()
            ]
        );
    }
}