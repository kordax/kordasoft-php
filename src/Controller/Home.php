<?php

namespace App\Controller;

use App\Utils\TranslationUtils;
use Mobile_Detect;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class Home extends AbstractController {
    /**
     * @Route("/", name="home")
     * @param TranslatorInterface $translator
     * @param Request $request
     * @return Response
     */
    public function route(TranslatorInterface $translator, Request $request) {
        $translations = TranslationUtils::translate(TranslationUtils::$_translations, $translator, $request->getLocale());
        $specific_translations =
            [
                'home.image_text',
                'home.services.top.text',
                'home.services.middle.text',
                'home.services.description.engineering.header',
                'home.services.description.outsourcing.header',
                'home.services.description.engineering.text',
                'home.services.description.outsourcing.text',
                'home.about.about-us',
                'home.about.team',
                'home.about.location',
                'home.about.advantages',
                'home.logo.text',
                'home.why.header',
                'home.why.text',
                'description.team.header.text',
                'description.team.text',
                'description.team.middle.text',
                "description.location.header",
                "description.location.text"
            ];
        $translations = array_merge(
            $translations,
            TranslationUtils::translate(
                $specific_translations,
                $translator,
                $request->getLocale()
            )
        );

        $mobileDetector = new Mobile_Detect;

        if ($mobileDetector->isMobile()) {
            $translations["mobile"] = true;
        } else {
            $translations["mobile"] = false;
        }

        return $this->render('home/home.html.twig', $translations);
    }
}