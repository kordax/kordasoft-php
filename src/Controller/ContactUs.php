<?php

namespace App\Controller;

use App\Utils\TranslationUtils;
use Mobile_Detect;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactUs extends AbstractController {

    /**
     * @Route("/contact-us", name="contact-us")
     * @param LoggerInterface $logger
     * @param MailerInterface $mailer
     * @param Request $request
     * @return Response
     */
    public function route(LoggerInterface $logger, TranslatorInterface $translator, Request $request) {
        $translations = [];
        foreach (TranslationUtils::$_translations as $translation) {
            $translations[$translation] = $translator->trans($translation, [], null, $request->getLocale());
        }
        $translations['locale'] = $request->getLocale();

        $translations['contact_us.ask_form.result.ok'] = $translator->trans('contact_us.ask_form.result.ok', [], null, $request->getLocale());
        $translations['contact_us.ask_form.result.nok'] = $translator->trans('contact_us.ask_form.result.nok', [], null, $request->getLocale());

        $mobileDetector = new Mobile_Detect;

        if ($mobileDetector->isMobile()) {
            $translations["mobile"] = true;
        } else {
            $translations["mobile"] = false;
        }

        $result = $request->query->get('result');
        if ($result == 'ok') {
            return $this->render('form/submit_ok.twig', $translations);
        } else if ($result == 'nok') {
            $translations['error_message'] = $request->query->get('error_message');
            return $this->render('form/submit_nok.twig', $translations);
        }

        $specific_translations =
            [
                'contact_us.header_text',
                'contact_us.ask_form.title',
                'contact_us.ask_form.service_type.title',
                'contact_us.ask_form.service_type.support',
                'contact_us.ask_form.service_type.qa',
                'contact_us.ask_form.service_type.development',
                'contact_us.ask_form.budget.title',
                'contact_us.ask_form.info.title',
                'contact_us.ask_form.name',
                'contact_us.ask_form.email',
                'contact_us.ask_form.company',
                'contact_us.ask_form.phone',
                'contact_us.ask_form.position',
                'contact_us.ask_form.website',
                'contact_us.ask_form.send_button_value',
                'contact_us.ask_form.service_type.outsourcing',
                'contact_us.email.success',
                'contact_us.email.error',
                'contact_us.ask_form.result.ok',
                'contact_us.ask_form.result.nok'
            ];
        $translations = array_merge(
            $translations,
            TranslationUtils::translate(
                $specific_translations,
                $translator,
                $request->getLocale()
            )
        );

        return $this->render('contacts/contact_us.html.twig', $translations);
    }
}