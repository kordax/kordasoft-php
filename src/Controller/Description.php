<?php

namespace App\Controller;

use App\Utils\TranslationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class Description extends AbstractController {

    /**
     * @Route("/description", name="description")
     * @param TranslatorInterface $translator
     * @param Request $request
     * @return Response
     */
    public function route(TranslatorInterface $translator, Request $request) {
        $translations = TranslationUtils::translate(TranslationUtils::$_translations, $translator, $request->getLocale());
        $specific_translations =
            [
                'description.team.header.text',
                'description.team.text',
                'description.team.middle.text',
                "description.location.header",
                "description.location.text"
            ];
        $translations = array_merge(
            $translations,
            TranslationUtils::translate(
                $specific_translations,
                $translator,
                $request->getLocale()
            )
        );

        return $this->render('description/description.html.twig', $translations);
    }
}