#!/bin/bash
composer install --no-dev --optimize-autoloader
APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
scp -r * kordax@valoru-software.com:/home/k/kordax/valoru-software.com/current/
